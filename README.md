Ansible Archlinux Fail2ban
==========================

A role to set basic fail2ban config

Tested and Used on ArchLinux but it may work on any Linux

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Should test with more jail (maybe use the jail.d directory)
- Be Kind
- Should check if ipv6 work as expected
